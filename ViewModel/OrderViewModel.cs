﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CM.Models;

namespace CM.ViewModel
{
    public class OrderViewModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public DateTimeOffset OrderDate { get; set; }
        public string ShipTo { get; set; }
        public Address ShippingAddress { get; set; }
        public decimal? GrandTotal { get; set; }
        public string ProductName { get; set; }
        public decimal? UnitPrice { get; set; }
        public string PaymentMethod { get; set; }
    }
}