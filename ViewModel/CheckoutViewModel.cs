﻿using CM.Models;
using System.Collections.Generic;

namespace CM.ViewModel
{
    public class CheckoutViewModel
    {
        public List<Address> Addresses { get; set; }
        public List<DisplaySelectedItem> OrderItems { get; set; }
        public List<Card> Cards { get; set; }
       
    }
}