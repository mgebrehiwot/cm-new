﻿using CM.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CM.ViewModel
{
    public class DisplaySelectedItem  : IEqualityComparer<DisplaySelectedItem>
    {
        private List<DisplaySelectedItem> _selectedItemsForDisplay;
        private List<DisplaySelectedItem> _savedItemsForLater;

        public DisplaySelectedItem()
        {
            _selectedItemsForDisplay = new List<DisplaySelectedItem>();
            _savedItemsForLater = new List<DisplaySelectedItem>();
            SavedItemsForLater = new List<DisplaySelectedItem>();
            DisplaySelectedItems = new List<DisplaySelectedItem>();
        }
        public int Id { get; set; }
        public int  ProductId { get; set; }
        public string PName { get; set; }
        public string PDescription { get; set; }
        public decimal? PPrice { get; set; }
        public int PQuantity { get; set; }
        public int SelectedItemId { get; set; }
        public double Tax { get; set; }
        public List<DisplaySelectedItem> SavedItemsForLater { get; set; }
        public List<DisplaySelectedItem> DisplaySelectedItems { get; set; }

        //public List<DisplaySelectedItem> GetSelectedItemsForDisplay(string userId, object status)
        //{
        //    DisplaySelectedItem displaySelectedItem = new DisplaySelectedItem();
        //    ApplicationUser applicationUser = new ApplicationUser();
        //    userId = applicationUser.Id;
        //    SelectedItem selectedItem = new SelectedItem {Status = SelectedItemStatus.intTheCart};
        //    status = (int)selectedItem.Status;
        //    SqlParameter parameterUserId  = new SqlParameter("@UserId",userId);
        //    SqlParameter parameterStatus = new SqlParameter("@Status", status);
        //    string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(cs))
        //    {
        //        SqlCommand cmd = new SqlCommand("sp_DisplaySelectedItems", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.Add(parameterUserId);
        //        cmd.Parameters.Add(parameterStatus);
        //        con.Open();
        //        SqlDataReader rdr = cmd.ExecuteReader();

        //        while (rdr.Read())
        //        {
        //            displaySelectedItem.PName = (string) rdr["Name"];
        //            displaySelectedItem.PDescription = (string) rdr["ProductDescription"];
        //            displaySelectedItem.PPrice = Convert.ToDecimal(rdr["CurrentPrice"]);
        //            displaySelectedItem.PQuantity = Convert.ToInt32(rdr["quantity"]);
        //            displaySelectedItem.SelectedItemId = Convert.ToInt32(rdr["SelectedItemId"]);
        //            _selectedItemsForDisplay.Add(displaySelectedItem);
        //        }
        //    }
        //    return _selectedItemsForDisplay;
        //}

        public bool Equals(DisplaySelectedItem x, DisplaySelectedItem y)
        {
            bool result = x.ProductId == y.ProductId;
            return result;
        }

        public int GetHashCode(DisplaySelectedItem obj)
        {
            return obj.ProductId.GetHashCode();
        }
    }
}