namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedUserColumnInSelectedItemsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SelectedItems", "UserId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SelectedItems", "UserId");
        }
    }
}
