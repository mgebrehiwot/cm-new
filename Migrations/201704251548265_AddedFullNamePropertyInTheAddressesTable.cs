namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFullNamePropertyInTheAddressesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "FullName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Addresses", "FullName");
        }
    }
}
