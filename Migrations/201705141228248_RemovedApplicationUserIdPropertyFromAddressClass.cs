namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedApplicationUserIdPropertyFromAddressClass : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Addresses", new[] { "ApplicationUser_Id1" });
            DropColumn("dbo.Addresses", "ApplicationUser_Id");
            RenameColumn(table: "dbo.Addresses", name: "ApplicationUser_Id1", newName: "ApplicationUser_Id");
            AlterColumn("dbo.Addresses", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Addresses", "ApplicationUser_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Addresses", new[] { "ApplicationUser_Id" });
            AlterColumn("dbo.Addresses", "ApplicationUser_Id", c => c.String(nullable: false));
            RenameColumn(table: "dbo.Addresses", name: "ApplicationUser_Id", newName: "ApplicationUser_Id1");
            AddColumn("dbo.Addresses", "ApplicationUser_Id", c => c.String(nullable: false));
            CreateIndex("dbo.Addresses", "ApplicationUser_Id1");
        }
    }
}
