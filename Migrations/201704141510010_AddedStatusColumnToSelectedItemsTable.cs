namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStatusColumnToSelectedItemsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SelectedItems", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SelectedItems", "Status");
        }
    }
}
