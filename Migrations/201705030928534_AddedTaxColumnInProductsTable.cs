namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTaxColumnInProductsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Tax", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Tax");
        }
    }
}
