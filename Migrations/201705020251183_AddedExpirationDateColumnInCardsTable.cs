namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedExpirationDateColumnInCardsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cards", "ExpirationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cards", "ExpirationDate");
        }
    }
}
