namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OverriddenConvention : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "Name", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Products", "ProductDescription", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Products", "CurrentPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "CurrentPrice", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Products", "ProductDescription", c => c.String());
            AlterColumn("dbo.Products", "Name", c => c.String());
        }
    }
}
