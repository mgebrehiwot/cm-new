namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedSelectedItemIdToOrderItemsInOrdersTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SelectedItems", "Order_OrderId", c => c.Int());
            CreateIndex("dbo.SelectedItems", "Order_OrderId");
            AddForeignKey("dbo.SelectedItems", "Order_OrderId", "dbo.Orders", "OrderId");
            DropColumn("dbo.Orders", "SeletedItemId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "SeletedItemId", c => c.Int(nullable: false));
            DropForeignKey("dbo.SelectedItems", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.SelectedItems", new[] { "Order_OrderId" });
            DropColumn("dbo.SelectedItems", "Order_OrderId");
        }
    }
}
