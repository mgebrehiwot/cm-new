namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedOrdersTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ApplicationUser_Id", c => c.String(nullable: false));
            DropColumn("dbo.Orders", "CustomerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "CustomerId", c => c.Int(nullable: false));
            DropColumn("dbo.Orders", "ApplicationUser_Id");
        }
    }
}
