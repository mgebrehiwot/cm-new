namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedSelectedItemsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SelectedItems", "ProductId", c => c.Int(nullable: false));
            AddColumn("dbo.SelectedItems", "Quantity", c => c.Int(nullable: false));
            DropColumn("dbo.SelectedItems", "ProductName");
            DropColumn("dbo.SelectedItems", "ProductDescription");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SelectedItems", "ProductDescription", c => c.String(nullable: false));
            AddColumn("dbo.SelectedItems", "ProductName", c => c.String(nullable: false));
            DropColumn("dbo.SelectedItems", "Quantity");
            DropColumn("dbo.SelectedItems", "ProductId");
        }
    }
}
