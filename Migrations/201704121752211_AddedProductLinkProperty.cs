namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProductLinkProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "ProductLink", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "ProductLink");
        }
    }
}
