namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedCardsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        CardId = c.Int(nullable: false, identity: true),
                        CardNumber = c.Int(nullable: false),
                        NameOnCard = c.String(),
                        ApplicationUser_Id = c.String(),
                    })
                .PrimaryKey(t => t.CardId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Cards");
        }
    }
}
