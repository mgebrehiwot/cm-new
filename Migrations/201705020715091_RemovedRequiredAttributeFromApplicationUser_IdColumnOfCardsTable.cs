namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedRequiredAttributeFromApplicationUser_IdColumnOfCardsTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cards", "ApplicationUser_Id", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cards", "ApplicationUser_Id", c => c.String(nullable: false));
        }
    }
}
