namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRequiredAttributeToCardsTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cards", "NameOnCard", c => c.String(nullable: false));
            AlterColumn("dbo.Cards", "ApplicationUser_Id", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cards", "ApplicationUser_Id", c => c.String());
            AlterColumn("dbo.Cards", "NameOnCard", c => c.String());
        }
    }
}
