namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUseThisAddressPropertyToAddressesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "UseThisAddress", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Addresses", "UseThisAddress");
        }
    }
}
