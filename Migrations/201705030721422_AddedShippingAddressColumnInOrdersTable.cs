namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedShippingAddressColumnInOrdersTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ShippingAddressId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "ShippingAddressId");
        }
    }
}
