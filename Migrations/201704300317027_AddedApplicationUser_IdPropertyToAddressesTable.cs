namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedApplicationUser_IdPropertyToAddressesTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Addresses", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Addresses", new[] { "ApplicationUser_Id" });
            AddColumn("dbo.Addresses", "ApplicationUser_Id1", c => c.String(maxLength: 128));
            AlterColumn("dbo.Addresses", "ApplicationUser_Id", c => c.String(nullable: false));
            CreateIndex("dbo.Addresses", "ApplicationUser_Id1");
            AddForeignKey("dbo.Addresses", "ApplicationUser_Id1", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Addresses", "ApplicationUser_Id1", "dbo.AspNetUsers");
            DropIndex("dbo.Addresses", new[] { "ApplicationUser_Id1" });
            AlterColumn("dbo.Addresses", "ApplicationUser_Id", c => c.String(maxLength: 128));
            DropColumn("dbo.Addresses", "ApplicationUser_Id1");
            CreateIndex("dbo.Addresses", "ApplicationUser_Id");
            AddForeignKey("dbo.Addresses", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
