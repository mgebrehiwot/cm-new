namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedStringLengthAttributeToMax_ProductDescription : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "ProductDescription", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "ProductDescription", c => c.String(nullable: false, maxLength: 255));
        }
    }
}
