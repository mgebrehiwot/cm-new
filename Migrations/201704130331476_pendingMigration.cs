namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pendingMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "SelectedItem_SelectedItemId", "dbo.SelectedItems");
            DropIndex("dbo.Products", new[] { "SelectedItem_SelectedItemId" });
            DropColumn("dbo.Products", "SelectedItem_SelectedItemId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "SelectedItem_SelectedItemId", c => c.Int());
            CreateIndex("dbo.Products", "SelectedItem_SelectedItemId");
            AddForeignKey("dbo.Products", "SelectedItem_SelectedItemId", "dbo.SelectedItems", "SelectedItemId");
        }
    }
}
