namespace CM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAddressesPropertyToApplicationUserAndRemovedAddressIdProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Addresses", "ApplicationUser_Id");
            AddForeignKey("dbo.Addresses", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.AspNetUsers", "AddressId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "AddressId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Addresses", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Addresses", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.Addresses", "ApplicationUser_Id");
        }
    }
}
