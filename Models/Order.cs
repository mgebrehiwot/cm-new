﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CM.ViewModel;

namespace CM.Models
{
    public class Order
    {
        public int OrderId { get; set; }

        [Required]
        public string ApplicationUser_Id { get; set; }

        [Required]
        public int CardId { get; set; }

        [Required]
        public int ShippingAddressId { get; set; }

        [Required]
        public DateTimeOffset OrderDate { get; set; }

        [Required]
        public List<SelectedItem> OrderItems { get; set; }

        public decimal? Tax { get; set; }

        [Required]
        public decimal? GrandTotal { get; set; }
        public int ProductId { get; set; }
    }
}