﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace CM.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
   

        
        
        public DbSet<Product> Products { get; set; }
        public DbSet<SelectedItem> SelectedItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Card> Cards { get; set; }
        public ApplicationDbContext()
            : base("ApplicationDbContext", throwIfV1Schema: false)
        {
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}