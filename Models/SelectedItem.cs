﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CM.Models
{
    public enum SelectedItemStatus
    {
        intTheCart,
        savedForLater, 
        deleted

  
    }
    
    public class SelectedItem: IEqualityComparer<SelectedItem>
    {
        public int SelectedItemId { get; set; }

        [Required]
        public int ProductId { get; set; }

        [Required]
        public decimal? PurchasePrice { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public string UserId { get; set; }

        public SelectedItemStatus Status { get; set; }
        public bool Equals(SelectedItem x, SelectedItem y)
        {
           bool result = x.ProductId == y.ProductId;
           return result;
        }

        public int GetHashCode(SelectedItem obj)
        {

            return obj.GetHashCode();
        }
    }

}