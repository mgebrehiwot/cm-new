﻿using System.ComponentModel.DataAnnotations;

namespace CM.Models
{
    public class Address
    {
        public int AddressId { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public string StreetLine1 { get; set; }

        public string StreetLine2 { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string ZipCode { get; set; }

        public bool UseThisAddress { get; set; }

        //[Required]
        //public string ApplicationUser_Id { get; set; }
    }
}