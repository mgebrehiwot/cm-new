﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CM.Models
{

   
    public class Card
    {
        public int CardId { get; set; }

        [Required]
        public DateTime ExpirationDate { get; set; }

        [Required]
        public int CardNumber { get; set; }

        [Required]
        public string NameOnCard { get; set; }

        public string ApplicationUser_Id { get; set; }
    }
}