﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CM.Models
{
    public class Product
    {
        public int ProductId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [DisplayName("Description")]
        public string ProductDescription { get; set; }

        [Required]
        [DisplayName("Price")]
        public decimal? CurrentPrice { get; set; }

        public string ProductLink { get; set; }
        public double Tax { get; set; }
        public static void UpdateProduct(Product product)
        {
            var cs = ConfigurationManager.ConnectionStrings["ApplicationDbContext"].ConnectionString;
            using (var con = new SqlConnection(cs))
            {
                var cmd = new SqlCommand("sp_UpdateProduct", con)
                {
                    CommandType = CommandType.StoredProcedure,
                };

                var productIdParameter = new SqlParameter()
                {
                    ParameterName = "@ProductId",
                    Value = product.ProductId
                };
                cmd.Parameters.Add(productIdParameter);

                var nameParameter = new SqlParameter()
                {
                    ParameterName = "@Name",
                    Value = product.Name
                };
                cmd.Parameters.Add(nameParameter);

                var descriptionParameter = new SqlParameter()
                {
                    ParameterName = "@ProductDescription",
                    Value = product.ProductDescription
                };
                cmd.Parameters.Add(descriptionParameter);

                var priceParameter = new SqlParameter()
                {
                    ParameterName = "@CurrentPrice",
                    Value = product.CurrentPrice
                };
                cmd.Parameters.Add(priceParameter);

                var linkParameter = new SqlParameter()
                {
                    ParameterName = "@ProductLink",
                    Value = product.ProductLink
                };
                cmd.Parameters.Add(linkParameter);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }
}