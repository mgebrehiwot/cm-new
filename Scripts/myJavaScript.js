﻿/// <reference path="jquery-1.10.2.js" />
    $(document).ready(function () {
        $(".quantity").off("change").on("change", function () {
            var totalBeforeTax = 0.0;
            var total = 0.0;
            var tax = 0.0;
            $(".quantity").each(function (index, elem) {
                var p = parseInt($(elem).siblings(".price").text());
                totalBeforeTax = totalBeforeTax + p * parseInt($(elem).val());
                tax = totalBeforeTax * .1;
                total = totalBeforeTax + tax;
            });
            $("#subTotal").text("$" + totalBeforeTax.toFixed(2));
            $("#totalBeforeTax").text("$" + totalBeforeTax.toFixed(2));
            $("#tax").text("$" + tax.toFixed(2));
            $("#total").text("$" + total.toFixed(2));
        });

    });

