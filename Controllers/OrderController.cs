﻿using CM.Models;
using CM.ViewModel;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CM.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private ApplicationDbContext _context;
        private List<OrderViewModel> _orderViewModelLst;

        public OrderController()
        {
            _context = new ApplicationDbContext();
            _orderViewModelLst = new List<OrderViewModel>();
        }
        public ActionResult PlaceOrder(CheckoutViewModel checkoutViewModel)
        {

            return View();
        }
        public ActionResult Orders()
        {
            string userId = User.Identity.GetUserId();
            IEnumerable<Order> orders = _context.Orders.Where(o => o.ApplicationUser_Id == userId).ToList();
            foreach (var order in orders)
            {
                var product = _context.Products.Single(p => p.ProductId == order.ProductId);
                var address = _context.Addresses.Single(a => a.AddressId == order.ShippingAddressId);
                OrderViewModel orderViewModel = new OrderViewModel()
                {
                    OrderId = order.OrderId,
                    OrderDate = order.OrderDate,
                    ProductId = order.ProductId,
                    GrandTotal = order.GrandTotal,
                    PaymentMethod = order.CardId.ToString(),
                    ProductName = product.Name,
                    ShippingAddress = address,
                    ShipTo = address.FullName,
                    UnitPrice = product.CurrentPrice
                    
                }; 
                _orderViewModelLst.Add(orderViewModel);
            }
            return View(_orderViewModelLst);
        }
        public ActionResult CancelOrder()
        {
            return View();
        }
        public ActionResult CancelItem()
        {
            return View();
        }
        public ActionResult TrackPackage()
        {
            return View();
        }
        public ActionResult ViewEditOrder()
        {
            return View();
        }

        private List<Address> GetAddresses(string userId)
        {
            List<Address> addressList = new List<Address>();
            if (_context.Addresses.Count() != 0)
            {
                addressList = _context.Addresses.ToList();

            }

            return addressList;
        }
        private List<Card> GetCards(string userId)
        {
            List<Card> cards = new List<Card>();
            if (_context.Cards.Count() != 0)
            {
                cards = _context.Cards.ToList();
            }
            return cards;
        }
        private List<DisplaySelectedItem> GetOrderItems(string userId)
        {
            //List<SelectedItem> orderItems = _context.SelectedItems.Where(s => s.Status == SelectedItemStatus.intTheCart && s.UserId == userId).ToList();
            var orderItems = new List<DisplaySelectedItem>();
            var queryItems = from s in _context.SelectedItems
                             join p in _context.Products
                             on s.ProductId equals p.ProductId
                             where s.Status == 0
                             where s.UserId == userId
                             select new
                             {
                                 productName = p.Name,
                                 productDescription = p.ProductDescription,
                                 price = p.CurrentPrice,
                                 tax = p.Tax

                             };
            foreach (var queryItem in queryItems)
            {
                DisplaySelectedItem displaySelectedItem = new DisplaySelectedItem();
                displaySelectedItem.PName = queryItem.productName;
                displaySelectedItem.PDescription = queryItem.productDescription;
                displaySelectedItem.PPrice = queryItem.price;
                displaySelectedItem.Tax = queryItem.tax;
                orderItems.Add(displaySelectedItem);

            }
            return orderItems;
        }
    }
}