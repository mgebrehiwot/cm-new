﻿using CM.Models;
using CM.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace CM.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
       
        private ApplicationDbContext _context;
        public bool IsAuthorizedUser;
        public List<DisplaySelectedItem> _inTheCart;
        private List<DisplaySelectedItem> _forLater;

        #region Constructor
        public ProductController()
        {
            _context = new ApplicationDbContext();
            _inTheCart = new List<DisplaySelectedItem>();
            _forLater = new List<DisplaySelectedItem>();
        }
        
        #endregion

        #region Action Methods

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetAllProducts()
        {
            var products = _context.Products.ToList();
            return View(products);
        }
        [HttpGet]
        public ActionResult ProductDetail(int productId)
        {
            var product = _context.Products.Single(p => p.ProductId == productId);
            
            return View(product);
        }


        [HttpGet]
        public ActionResult AddProduct()
        {
            //var admin = _context.Users.Single(u => u.Email == "mgebrehiwot@gmail.com");
            //var currentUserId = User.Identity.GetUserId();
            //var currentUser = _context.Users.Single(u => u.Id == currentUserId);
            //IsAuthorizedUser = admin == currentUser;
            //if (IsAuthorizedUser)
            //{
            return View();
            //}
            //return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public ActionResult AddProduct(Product productObject)
        {
            if (ModelState.IsValid)
            {
                var product = new Product()
                {
                    //ProductId = productObject.ProductId,
                    Name = productObject.Name,
                    ProductDescription = productObject.ProductDescription,
                    CurrentPrice = productObject.CurrentPrice,
                    ProductLink = productObject.ProductLink
                };
                _context.Products.Add(product);
                _context.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpGet]
        public ActionResult EditProduct(int id)
        {
            var admin = _context.Users.Single(adm => adm.Email == "mgebrehiwot@gmail.com");
            var userId = User.Identity.GetUserId();
            var currentUser = _context.Users.Single(u => u.Id == userId);
            bool isAdmin = admin.Id == currentUser.Id;
            if (isAdmin)
            {
                var product = _context.Products.Single(p => p.ProductId == id);
                return View(product);
            }
            return RedirectToAction("Error", "Product");

        }

        [HttpPost]
        public ActionResult EditProduct(Product product)
        {
            var admin = _context.Users.Single(adm => adm.Email == "mgebrehiwot@gmail.com");
            var userId = User.Identity.GetUserId();
            var currentUser = _context.Users.Single(u => u.Id == userId);
            bool isAdmin = admin.Id == currentUser.Id;
            if (isAdmin)
            {
                if (ModelState.IsValid)
                {
                    Product.UpdateProduct(product);
                    return RedirectToAction("GetAllProducts", "Product");
                }
                return View();

            }
            return RedirectToAction("Error", "Product");
        }

        [HttpGet]
        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Remove(int id)
        {
            _context.Products.Single(p => p.ProductId.Equals(id));
            return RedirectToAction("GetAllProducts", "Product");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult AddToCart(int id)
        {
            var userId = User.Identity.GetUserId();
            if (userId != null)
            {
                var product = _context.Products.Single(p => p.ProductId == id );
                var selectedItem = new SelectedItem
                {
                    PurchasePrice = product.CurrentPrice,
                    ProductId = product.ProductId,
                    Quantity = 1,
                    UserId = userId,
                    Status = SelectedItemStatus.intTheCart
                };
                InsertDistinctProduct(selectedItem);
            }
            else
            {
                var product = _context.Products.Single(p => p.ProductId == id);
                var selectedItem = new SelectedItem
                {
                    PurchasePrice = product.CurrentPrice,
                    ProductId = product.ProductId,
                    Quantity = 1,
                    UserId = "anonymous",
                    Status = SelectedItemStatus.intTheCart
                };
                InsertDistinctProduct(selectedItem);
            }
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Cart(string userId)
        {
            ViewResult view;                
            userId = User.Identity.GetUserId();
            if (IsAuthorizedUser)
            {
                var distinctItems = DisplaySelectedItems(userId);
                view = View(distinctItems);
            }
            else
            {
                var distinctItems = DisplaySelectedItems("anonymous");
                view = View(distinctItems);
            }
            return view;
        }

        [HttpGet]
        public ActionResult Delete(int selectedItemId)
        {
            var itemToDelete = _context.SelectedItems.Single(s => s.SelectedItemId == selectedItemId);
            itemToDelete.Status = SelectedItemStatus.deleted;
            _context.SaveChanges();
            return RedirectToAction("Cart", "Product");
        }

        [HttpGet]
        [ActionName("MoveToSaveForLater")]
        public ActionResult SaveForLater(int selectedItemId)
        {
            var itemToSaveForLater = _context.SelectedItems.Single(s => s.SelectedItemId == selectedItemId);
            itemToSaveForLater.Status = SelectedItemStatus.savedForLater;
            _context.SaveChanges();
            return RedirectToAction("SaveForLater", "Product");
        }

        [HttpGet]
        public ActionResult SaveForLater()
        {
            string userId = User.Identity.GetUserId();
            SelectedItem selectedItem = new SelectedItem { Status = SelectedItemStatus.savedForLater };
            int status = (int)selectedItem.Status;
            _forLater = GetValue(userId, status);
            return View(_forLater);
        }

        public ActionResult MoveToCart(int selectedItemId)
        {
            var itemToMoveToCart =
                _context.SelectedItems.Single(m => m.SelectedItemId == selectedItemId);
            itemToMoveToCart.Status = SelectedItemStatus.intTheCart;
            _context.SaveChanges();
            return RedirectToAction("Cart", "Product");
        }

        public ActionResult DeleteFromSaveForLaterList(int selectedItemId)
        {
            var itemToDeleteFromSaveForLaterList =
                _context.SelectedItems.Single(m => m.SelectedItemId == selectedItemId);
            itemToDeleteFromSaveForLaterList.Status = SelectedItemStatus.deleted;
            _context.SaveChanges();
            return View();
        }
        #endregion

        #region Helper methods
        public List<DisplaySelectedItem> DisplaySelectedItems(string userId)
        {
            var itmesInTheCart = from s in _context.SelectedItems
                                 join p in _context.Products
                                 on s.ProductId equals p.ProductId
                                 where s.Status == 0
                                 where s.UserId == userId
                                 select new
                                 {
                                     productId = p.ProductId,
                                     productName = p.Name,
                                     productDiscription = p.ProductDescription,
                                     productPrice = p.CurrentPrice,
                                     productLink = p.ProductLink,
                                     productQuantity = s.Quantity,
                                     userId = s.UserId,
                                     selectedItemId = s.SelectedItemId
                                 };
            foreach (var item in itmesInTheCart)
            {
                DisplaySelectedItem displaySelectedItem = new DisplaySelectedItem();
                displaySelectedItem.ProductId = item.productId;
                displaySelectedItem.PName = item.productName;
                displaySelectedItem.PDescription = item.productDiscription;
                displaySelectedItem.PPrice = item.productPrice;
                displaySelectedItem.PQuantity = item.productQuantity;
                displaySelectedItem.SelectedItemId = item.selectedItemId;
                _inTheCart.Add(displaySelectedItem);
            }
            List<DisplaySelectedItem> distinctItems = _inTheCart.Distinct(new DisplaySelectedItem()).ToList();
            return distinctItems;
        }
        private static List<DisplaySelectedItem> GetValue(string userId, int status)
        {
            var displaySelectedItems = new List<DisplaySelectedItem>();
            var parameterUserId = new SqlParameter("@UserId", userId);
            var parameterStatus = new SqlParameter("@Status", status);
            var cs = ConfigurationManager.ConnectionStrings["ApplicationDbContext"].ConnectionString;
            using (var con = new SqlConnection(cs))
            {
                var cmd = new SqlCommand("sp_DisplaySelectedItems", con) {CommandType = CommandType.StoredProcedure};
                cmd.Parameters.Add(parameterUserId);
                cmd.Parameters.Add(parameterStatus);
                con.Open();
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var displaySelectedItem = new DisplaySelectedItem
                    {
                        ProductId = Convert.ToInt32(rdr["ProductId"]),
                        PName = (string) rdr["Name"],
                        PDescription = (string) rdr["ProductDescription"],
                        PPrice = Convert.ToDecimal(rdr["CurrentPrice"]),
                        PQuantity = Convert.ToInt32(rdr["quantity"]),
                        SelectedItemId = Convert.ToInt32(rdr["SelectedItemId"])
                    };
                    displaySelectedItems.Add(displaySelectedItem);
                }
            }
            var distinctItems = displaySelectedItems.Distinct(new DisplaySelectedItem()).ToList();

            return distinctItems;
        }
        private void InsertDistinctProduct(SelectedItem selectedItem)
        {
            var itemsInSelectedItems = _context.SelectedItems.ToList();
            if (itemsInSelectedItems.Count == 0)
            {
                _context.SelectedItems.Add(selectedItem);
                _context.SaveChanges();
            }
            else
            {
                int itemCount = 0;
                foreach (var item in itemsInSelectedItems)
                {
                    if (selectedItem.ProductId == item.ProductId)
                    {
                        item.Status = SelectedItemStatus.intTheCart;
                        _context.SaveChanges();
                        itemCount++;                      
                    }
                }
                if (itemCount == 0)
                {
                    _context.SelectedItems.Add(selectedItem);
                    _context.SaveChanges();
                }
            }
        }

        #endregion
    }
}