﻿using CM.Models;
using System.Web.Mvc;
using CM.ViewModel;
using Microsoft.AspNet.Identity;

namespace CM.Controllers
{
    public class CardController : Controller
    {
        private ApplicationDbContext _context;

        public CardController()
        {
            _context = new ApplicationDbContext();
        }

        [Authorize]
        [HttpGet]
        [ActionName("AddCard")]
        public ActionResult AddCard_Get()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ActionName("AddCard")]
        public ActionResult AddCard_Post(Card cardObj)
        {
            Card card = new Card()
            {
                ApplicationUser_Id = User.Identity.GetUserId(),
                CardNumber = cardObj.CardNumber,
                NameOnCard = cardObj.NameOnCard,
                ExpirationDate = cardObj.ExpirationDate
            };
            if (ModelState.IsValid)
            {
                _context.Cards.Add(card);
                _context.SaveChanges();
                return RedirectToAction("Checkout", "CheckOut");
            }
            return View();
        }
    }
}