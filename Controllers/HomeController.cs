﻿using CM.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;

namespace CM.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;
        //private static bool _authorized;
        //private string userId;
        public static int itemCountForUser = 0;
        public static int itemCountForAnonymous = 0;



        public HomeController()
        {
            var itemsInCartForUser = new List<SelectedItem>();
            var itemsInCartForAnonymous = new List<SelectedItem>();
            _context = new ApplicationDbContext();
            foreach (var sItem in _context.SelectedItems)
            {
                if (sItem.Status == 0)
                {
                    if (sItem.UserId != "anonymous")
                    {
                        itemsInCartForUser.Add(sItem);
                    }
                    else
                    {
                        itemsInCartForAnonymous.Add(sItem);
                    }
                }
            }
            itemCountForUser = itemsInCartForUser.Count;
            itemCountForAnonymous = itemsInCartForAnonymous.Count;
        }
        public ActionResult Index()
        {
            var products = _context.Products.ToList();
            return View(products);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}