﻿using CM.Models;
using CM.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CM.Controllers
{
    public class CheckOutController : Controller
    {
        private ApplicationDbContext _context;


        [HttpGet]
        [ActionName("AddCard")]
        public ActionResult AddCard_Get(string userId)
        {
            return View();
        }

        [HttpPost]
        [ActionName("AddCard")]
        public ActionResult AddCard_Post(Card cardObj)
        {
            if (ModelState.IsValid)
            {
                Card card = new Card()
                {
                    ApplicationUser_Id = cardObj.ApplicationUser_Id,
                    CardNumber = cardObj.CardNumber,
                    NameOnCard = cardObj.NameOnCard,
                    ExpirationDate = cardObj.ExpirationDate
                };
                _context.Cards.Add(card);
            _context.SaveChanges();
            return RedirectToAction("Checkout", "CheckOut");
            }
            return View();

        }
        public CheckOutController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpGet]
        [ActionName("Checkout")]
        public ActionResult Checkout_Get()
        {
            CheckoutViewModel checkoutViewModel = new CheckoutViewModel();
            ActionResult view;
            var userId = User.Identity.GetUserId();
            if (userId != null)
            {
                checkoutViewModel.Addresses = GetAddresses(userId);
                checkoutViewModel.Cards = GetCards(userId);
                checkoutViewModel.OrderItems = GetOrderItems(userId);
                view = View(checkoutViewModel);
            }
            else
            {
                checkoutViewModel.Addresses = GetAddresses("anonymous");
                checkoutViewModel.Cards = GetCards("anonymous");
                checkoutViewModel.OrderItems = GetOrderItems("anonymous");
                view = View(checkoutViewModel);
            }
            return view;
        }

        [HttpPost]
        [ActionName("Checkout")]
        public ActionResult Checkout_Post(int addressId, int cardId, int productId, int? quantity)
        {
            Product product = _context.Products.Single(p => p.ProductId == productId);
            decimal? subtotal = product.CurrentPrice.Value * quantity.Value;
            decimal? tax = subtotal*.1M;
            decimal? grandtotal = subtotal + tax;
            var itemInCart = _context.SelectedItems.Single(s => s.ProductId == productId);
            itemInCart.Status = SelectedItemStatus.deleted;

            Order order = new Order()
            {
                OrderDate = DateTimeOffset.Now,
                ApplicationUser_Id = User.Identity.GetUserId(),
                CardId = cardId,
                GrandTotal = grandtotal,
                Tax = .1M,
                ShippingAddressId = addressId,
                OrderItems = _context.SelectedItems.Where(s => s.Status == SelectedItemStatus.intTheCart).ToList(),
                ProductId = productId
            };
            _context.Orders.Add(order);
            _context.SaveChanges();
            return RedirectToAction("PlaceOrder", "Order");
            
        }

        [HttpGet]
        [ActionName("AddAddress")]
        public ActionResult AddAddress_Get()
        {
            return View();
        }     
        
        [HttpPost]
        [ActionName("AddAddress")]
        public ActionResult AddAddress_Post(Address address, string userId)
        {
            userId = User.Identity.GetUserId();
            Address ad = new Address()
            {
                FullName = address.FullName,
                City = address.City,
                Country = address.Country,
                State = address.State,
                StreetLine1 = address.StreetLine1,
                ZipCode = address.ZipCode
                //ApplicationUser_Id = userId
            };
            if (ModelState.IsValid)
            {
                _context.Addresses.Add(ad);
                _context.SaveChanges();
                return RedirectToAction("Checkout", "CheckOut");
            }
            return View();
        }

        [HttpGet]
        [ActionName("EditAddress")]
        public ActionResult EditAddress_Get(int addressId)
        {
            var address = _context.Addresses.Single(ad => ad.AddressId == addressId);
            return View(address);
        }

        [HttpPost]
        [ActionName("EditAddress")]
        public ActionResult EditAddress_Post(Address addressObj)
        {
           //r address = _context.Addresses.Single(ad => ad.AddressId.ToString() == "1");

            // Query the database for the row to be updated.
            var query =
                from address in _context.Addresses
                where address.AddressId.ToString() == "1"
                select address;

            // Execute the query, and change the column values you want to change.
            foreach (Address ad in query)
            {
                //ad.ApplicationUser_Id = addressObj.ApplicationUser_Id;
                ad.FullName = addressObj.FullName;
                ad.StreetLine1 = addressObj.StreetLine1;
                ad.StreetLine2 = addressObj.StreetLine2;
                ad.City = addressObj.City;
                ad.State = addressObj.State;
                ad.Country = addressObj.Country;
            }

            // Submit the changes to the database.
            try
            {
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                
                // Provide for exceptions.
            }
            return RedirectToAction("Checkout", "CheckOut");
        }

        #region Helper method
        private List<Address> GetAddresses(string userId)
        {
            List<Address> addressList = new List<Address>();
            if (_context.Addresses.Count() != 0)
            {
                addressList = _context.Addresses.ToList();

            }

            return addressList;
        }
        private List<Card> GetCards(string userId)
        {
            List<Card> cards = new List<Card>();
            if (_context.Cards.Count() != 0)
            {
                cards = _context.Cards.Where(c => c.ApplicationUser_Id == userId).ToList();
            }
            return cards;
        }
        private List<DisplaySelectedItem> GetOrderItems(string userId)
        {
           //List<SelectedItem> orderItems = _context.SelectedItems.Where(s => s.Status == SelectedItemStatus.intTheCart && s.UserId == userId).ToList();
            var orderItems = new List<DisplaySelectedItem>();
           var queryItems = from s in _context.SelectedItems
                join p in _context.Products
                on s.ProductId equals p.ProductId
                where s.Status == 0
                where s.UserId == userId
                select new
                {
                    productId = p.ProductId,
                    productName = p.Name,
                    productDescription = p.ProductDescription,
                    price = p.CurrentPrice,
                    tax = p.Tax

                };
            foreach (var queryItem in queryItems)
            {
                DisplaySelectedItem displaySelectedItem = new DisplaySelectedItem();
                displaySelectedItem.ProductId = queryItem.productId;
                displaySelectedItem.PName = queryItem.productName;
                displaySelectedItem.PDescription = queryItem.productDescription;
                displaySelectedItem.PPrice = queryItem.price;
                displaySelectedItem.Tax = queryItem.tax;
                orderItems.Add(displaySelectedItem);

            }
            return orderItems;
        }
	    #endregion    
    }
}